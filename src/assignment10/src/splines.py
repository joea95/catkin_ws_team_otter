#!/usr/bin/env python
# coding=utf-8
import rospy
import scipy.interpolate
import numpy as np
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point,Pose,Quaternion,PointStamped
from std_msgs.msg import Header
from std_msgs.msg import ColorRGBA
import threading

class Map():

    def __init__(self,samples,id,num_points=20):
        self.id=id
        size=len(samples)
        step=size//num_points
        indices=[i*step for i in range(num_points)]
        indices.append((len(samples)-1))
        self.samples=samples
        print(indices)
        self.x_spline=scipy.interpolate.CubicSpline(samples[indices,0],samples[indices,1])
        self.y_spline=scipy.interpolate.CubicSpline(samples[indices,0],samples[indices,2])

        self.publisher=rospy.Publisher('/visualization/map', Marker,queue_size=10)
        #rospy.init_node("splineMap",anonymous=True)

    def closestPoint(self,p,lookahead=0.0,initial_step=0.1,cutoff_change=0.001):

        target=np.array([p.x,p.y])
        max_u=self.samples[-1,0]
        u=0
        distance=np.linalg.norm([self.x_spline(u),self.y_spline(u)]-target)
        u=initial_step
        pos_distance=np.linalg.norm([self.x_spline(u),self.y_spline(u)]-target)
        u=max_u-initial_step
        neg_distance=np.linalg.norm([self.x_spline(u),self.y_spline(u)]-target)
        if neg_distance<distance:
            step=-initial_step
            change=distance-neg_distance
            prev_distance=neg_distance
        else:
            step=initial_step
            u=initial_step
            change=distance-pos_distance
            prev_distance=pos_distance
        u+=step
        while abs(change)>cutoff_change:
            distance=np.linalg.norm([self.x_spline(u),self.y_spline(u)]-target)
            change=prev_distance-distance
            prev_distance=distance
            if change<0:
                step=-step/2
            u+=step
        u+=lookahead
        if u>max_u:
            u-=max_u
        return Point(self.x_spline(u),self.y_spline(u),0)


    def show_closest_point(self,msg,lookahead=0.0):
        p=msg.point
        closest=self.closestPoint(p,lookahead=lookahead)

        print('closest Point',closest.x,closest.y)
        self.showMarker(closest)


    def showMarker(self,point):
        h=Header()
        h.stamp=rospy.Time.now()
        h.frame_id='map'
        marker=Marker()
        marker.header=h
        marker.ns='closestPoint'
        marker.id=self.id
        marker.type=Marker.SPHERE
        marker.action=Marker.ADD
        p=Pose(point,Quaternion(0.0,0.0,0.0,1.0))
        marker.pose=p
        marker.scale.x = 0.5
        marker.scale.y = 0.5
        marker.scale.z = 0.5
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.color.a = 1.0
        marker.lifetime = rospy.Duration()

        self.publisher.publish(marker)
