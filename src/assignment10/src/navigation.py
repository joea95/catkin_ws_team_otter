#!/usr/bin/env python
# coding=utf-8
import rospy
import math
import numpy as np
from splines import Map
from nav_msgs.msg import Odometry
from autominy_msgs.msg import SteeringCommand
from std_msgs.msg import UInt8

class Control():

    def __init__(self):
        self.lane1=Map(np.load('lane1.npy'),1)
        self.lane2=Map(np.load('lane2.npy'),2)

        self.target_lane=self.lane1


        rospy.init_node("control")
        rospy.Subscriber("/sensors/localization/filtered_map", Odometry, self.on_localization, queue_size=1)
        rospy.Subscriber("/control/lane", UInt8, self.on_lane, queue_size=1)
        self.steering_pub = rospy.Publisher("/control/steering", SteeringCommand, queue_size=10)

    def on_lane(self,msg):
        if msg.data==1:
            self.target_lane=self.lane1
        elif msg.data==2:
            self.target_lane=self.lane2

    def on_localization(self,msg):
        start=msg.pose.pose.position
        target=self.target_lane.closestPoint(start,lookahead=0.75)
        self.target_lane.showMarker(target)

        dx=target.x-start.x
        dy=target.y-start.y

        yaw=np.arctan2(dy,dx)

        com=SteeringCommand()
        com.value=yaw
        print (math.degrees(yaw))
        self.steering_pub.publish(com)

if __name__=='__main__':
    Control()
    rospy.spin()
