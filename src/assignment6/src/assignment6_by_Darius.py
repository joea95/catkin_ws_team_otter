#!/usr/bin/env python
import rospy
from sensor_msgs.msg import CameraInfo, Image
from cv_bridge import CvBridge
import cv2
import numpy as np
import random
import time

pub = rospy.Publisher('/sensors/camera/infra1/image_rect_raw/binary_image', Image, queue_size=10)
bridge = CvBridge()

def ransac(image, offset, iterations, distance):
    #select two random white dots
    #make sure they are not the same
    #make function y=mx+n
    #draw grey line
    #make line by d thicker, count remaining white pixels
    #print(cv2.countNonZero(images[0]), cv2.countNonZero(images[1]), cv2.countNonZero(images[2]))
    (_x1,_y1), (_x2,_y2), _m, _n, _whiteLeft = (0,0), (0,0), 0, 0, cv2.countNonZero(image)

    for i in range(0, iterations):
	imageCopy = image.copy()
	height, width = imageCopy.shape
	(x1,y1) = (0,0) # assumes that (0,0) is black
	(x2,y2) = (0,0)
	while(imageCopy[y1][x1] == 0):
	   (y1,x1) = (random.randint(1,height-1), random.randint(1,width-1))#may never happen
	while(imageCopy[y2][x2] == 0 and (x1,y1) != (x2,y2)):
	   (y2,x2) = (random.randint(1,height-1), random.randint(1,width-1))

	#linear equation y=mx+n

	m = float(y2-y1)/float(x2-x1)
	n = float(y1-(m*x1))

	cv2.line(imageCopy, (0,int(n)), (width,int(m*width+n)), 0, distance)

	whiteLeft = cv2.countNonZero(imageCopy)
	if whiteLeft < _whiteLeft:
	    (_x1,_y1), (_x2,_y2), _m, _n, _whiteLeft = (x1,y1), (x2,y2), m, n, whiteLeft

    _x1 += offset[0]
    _y1 += offset[1]
    _x2 += offset[0]
    _y2 += offset[1]
    _m = float(_y2-_y1)/float(_x2-_x1)
    _n = float(_y1-(_m*_x1))

    return((_x1,_y1), (_x2,_y2), _m, _n, _whiteLeft)

def callback(data):

    cv_image = bridge.imgmsg_to_cv2(data, desired_encoding="mono8")
    
    height, width = cv_image.shape

#####6-2 Lane segmentation

    #thresholding
    thresh = 215
    maxValue = 255
    
    _, dst = cv2.threshold(cv_image, thresh, maxValue, cv2.THRESH_BINARY);

    #overdraw non-lane white areas
    cv2.rectangle(dst, (0, 0), (width, 120), 0 , -1)
    cv2.rectangle(dst, (230, 300), (470, height), 0 , -1)
    cv2.rectangle(dst, (0, 0), (250, 200), 0 , -1)
    cv2.rectangle(dst, (0, 0), (150, height-100), 0 , -1)
    cv2.rectangle(dst, (450, height-100), (500, height), 0 , -1)
    cv2.rectangle(dst, (450, height-55), (525, height), 0 , -1)

    #seperate lanes
    dst2 = dst

    images=[
        dst[0:height, 0:355].copy(),
        dst[0:height, 355:width].copy(),
        dst[0:300, 430:width].copy()
    ]
    cv2.rectangle(images[1], (430-355, 0), (width, 300), 0 , -1)

    offsets=[
        [0,0],
        [355,0],
        [430,0]
    ]

#####6-3 Getting the line equation (RANSAC)
#call ransac 3x, give it the offsets and draw 3x lines
    linesToDraw=[]

    for i in range(0, len(images)):
	linesToDraw.append(ransac(images[i], offsets[i], 10, 1))

    for line in linesToDraw:
	cv2.rectangle(dst, (line[0][0]-5, line[0][1]-5), (line[0][0]+5, line[0][1]+5), 125 , -1)
	cv2.rectangle(dst, (line[1][0]-5, line[1][1]-5), (line[1][0]+5, line[1][1]+5), 125 , -1)
	cv2.line(dst, (0,int(line[3])), (width,int(line[2]*width+line[3])), 127, 1)
	print((line[0][0],line[0][1]), (line[1][0],line[1][1]), line[2], line[3], line[4])

    time.sleep(2)

    #visualize lane seperation
    #cv2.rectangle(dst, (0, 0), (355, height), 50 , 1)
    #cv2.rectangle(dst, (430, 0), (width, 300), 50 , 1)



    #publish
    image_message = bridge.cv2_to_imgmsg(dst, encoding="mono8")

    pub.publish(image_message)


def listener():
    rospy.init_node('binaryImage', anonymous=True)

    rospy.Subscriber('/sensors/camera/infra1/image_rect_raw', Image, callback)

    rospy.spin()

if __name__ == '__main__':
    listener()
