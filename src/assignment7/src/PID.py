#!/usr/bin/env python
# coding=utf-8
import rospy
from geometry_msgs.msg import PoseWithCovariance,Pose,Point,Quaternion
from autominy_msgs.msg import NormalizedSteeringCommand
from std_msgs.msg import Header
from nav_msgs.msg import Odometry
from tf.transformations import euler_from_quaternion
import numpy as np
import math

class PID():

    #yaw=0 := Richtung Pokale, yaw=pi := Richtung Tür-Seite
    def __init__(self,yaw_angle,car_id=8):
        print('init')
        self.angles=[]#enthält die Fehler
        self.wanted_yaw=yaw_angle
        self.steer_pub=rospy.Publisher('/actuators/steering_normalized', NormalizedSteeringCommand,queue_size=10)
        rospy.init_node("pid")
        rospy.Subscriber("/communication/gps/"+str(car_id),Odometry,self.onPosition)
        print('init finish')
        rospy.spin()

    def onPosition(self,odometry):
        quaternion_msg=odometry.pose.pose.orientation
        quaternion=(quaternion_msg.x,quaternion_msg.y,quaternion_msg.z,quaternion_msg.w)
        _,_,yaw=euler_from_quaternion(quaternion)

        #abl ist die Änderung des Fehlers, also die Ableitung
        try:
            abl=(self.wanted_yaw-yaw)-self.angles[-1]
        except:
            abl=0
        self.angles.append(self.wanted_yaw-yaw)

        print('yaw:',yaw)
        p,d,i=rospy.get_param("/pid/p"),rospy.get_param("/pid/d"),rospy.get_param("/pid/i")
        #u=  p*Fehler  +  d*Ableitung  +  i*Integral über alle bisherigen Fehler
        u=p*(self.wanted_yaw-yaw)+d*(abl)+i*np.trapz(np.asarray(self.angles))


        print('u:',u)

        #u als SteeringCommand publishen
        h=Header()
        h.stamp=rospy.Time.now()
        steer_com=NormalizedSteeringCommand(h,u)
        self.steer_pub.publish(steer_com)

if __name__=='__main__':
    PID(0)
