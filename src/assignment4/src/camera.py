#!/usr/bin/env python
# coding=utf-8
import rospy
import cv2 as cv
import numpy as np
from sensor_msgs.msg import CameraInfo,Image
from cv_bridge import CvBridge

class Camera():

    def __init__(self):
        self.intrinsic = np.zeros(1)
        self.distortion= np.zeros(1)
        self.image= None
        self.binary_image= None
        self.bridge=CvBridge()

    #callback wenn ein Bild gelesen wird
    def imageIn(self,data):
        self.image=self.bridge.imgmsg_to_cv2(data,"mono8")#umwandeln in cv
        _,binIm=cv.threshold(self.image,230,255,cv.THRESH_BINARY)
        #schwarze Dreiecke drüberzeichnen
        cv.rectangle(binIm,(0,0),(639,105),(0,0,0),-1)
        cv.rectangle(binIm,(0,245),(639,479),(0,0,0),-1)
        self.binary_image=binIm
        self.bin_pub.publish(self.bridge.cv2_to_imgmsg(binIm,"mono8"))#bild wieder publishen


    def cInfo(self,data):
        #CameraInfos in die richtige Form (für solvePnP) bringen und merken für später
        self.intrinsic=np.asarray(data.K).reshape(3,3)
        self.distortion=np.asarray(data.D).reshape(5,1)

    def main(self):
        self.bin_pub=rospy.Publisher('/image/binary',Image,queue_size=10)#publisht das binary_image
        rospy.init_node('assignment4', anonymous=True)
        rospy.Subscriber('/sensors/camera/infra1/camera_info', CameraInfo, self.cInfo)
        rospy.Subscriber('/sensors/camera/infra1/image_rect_raw', Image, self.imageIn)

        #rospy.spin()
        #rospy.sleep(1)
        #cv.imshow("im",self.image)
        #cv.waitKey()

#teilt das Bild in 6 Teile entlang der Achsen bei y=horizontal_1, y=horizontal_2 und x=vertical
def splitImage(img,vertical=340,horizontal_1=140,horizontal_2=200):
    height, width = img.shape[:2]
    images=[
        img[0:horizontal_1-1,0:vertical-1],
        img[0:horizontal_1-1,vertical:width],
        img[horizontal_1:horizontal_2-1,0:vertical-1],
        img[horizontal_1:horizontal_2-1,vertical:width],
        img[horizontal_2:height,0:vertical-1],
        img[horizontal_2:height,vertical:width],
    ]
    #offset vom 0,0 des Teilbildes zum Originalbild
    offsets=[
        [0,0],
        [0,vertical],
        [horizontal_1,0],
        [horizontal_1,vertical],
        [horizontal_2,0],
        [horizontal_2,vertical]
    ]
    return images,offsets

#berechent die durchschnittliche Position weißer Pixel
def calc_average_position(img):
    height, width = img.shape[:2]
    positions=[]
    y=-1
    for row in img:
        y+=1
        x=-1
        for pixel in row:
            x+=1
            if pixel >= 255:
                positions.append([y,x])
    return np.mean(positions,axis=0,dtype=np.int_)

#koordinaten der weißen Punkte (in 3D!, mit z=0, da die Punkte auf dem Boden liegen)
real_world_coord=np.asarray([#top left to bottom right
    [1.1,0.2,0],
    [1.1,-0.2,0],
    [0.8,0.2,0],
    [0.8,-0.2,0],
    [0.5,0.2,0],
    [0.5,-0.2,0],

]).reshape(6,3,1)#umformen für solvePnP

#errechnet Rotationmatrix,Translationsmatix, etc der Camera c und gibt sie aus
def print_matrices(c):
    images,offsets=splitImage(c.binary_image)#Bild aufteilen
    positions=[]
    for img in images:
        positions.append(calc_average_position(img))#Positionen (innerhalb des jeweiligen Teilbilds) der weißen Punkte errechnen

    #offset auf die Positionen addieren um Positionen um Gesamtbild zu erhalten und umformen für solvePnP
    positions=(np.asarray(positions,dtype=np.float)+np.asarray(offsets,dtype=np.float)).reshape(6,2,1)

    print("Marker positions:")
    print(positions)

    retval,rvec,tvec=cv.solvePnP(real_world_coord,positions,c.intrinsic,c.distortion)
    print("rvec:")
    print(rvec)
    print("tvec:")
    print(tvec)#=Translationsvektor

    rot_matrix,_=cv.Rodrigues(rvec)#Rotaionsmatrix errechnen
    print("Rotationmatrix:")
    print(rot_matrix)

    translationsmatix=np.append(np.append(rot_matrix,tvec,axis=1),[[0,0,0,1]],axis=0)#Translationsvektor und untere  Zeile an Rotationmatrix anhängen um Translationsmatix zu erhalten
    print("Translationmatrix:")
    print(translationsmatix)

    print("inverse")
    print(np.linalg.inv(translationsmatix))


if __name__=='__main__':

    c=Camera()
    c.main()
    rospy.sleep(1)#kurz warten um erste Nachrichten aus den Topics zu lesen
    print("intrinsic Matrix")
    print(c.intrinsic)
    print("distortion coefficients")
    print(c.distortion)
    print_matrices(c)
