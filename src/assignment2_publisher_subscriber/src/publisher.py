#!/usr/bin/env python
import rospy
from autominy_msgs.msg import SpeedCommand
from autominy_msgs.msg import SteeringCommand
from std_msgs.msg import Header
from autominy_msgs.msg import NormalizedSteeringCommand


def main():
    speed_pub=rospy.Publisher('/actuators/speed',SpeedCommand,queue_size=10)
    steer_pub=rospy.Publisher('/actuators/steering_normalized', NormalizedSteeringCommand,queue_size=10)
    rospy.init_node('assignment2',anonymous=True)
    rate = rospy.Rate(100)
    while not rospy.is_shutdown():
        h=Header()
        h.stamp=rospy.Time.now()
        speed_com=SpeedCommand(h,0.3)
        h=Header()
        h.stamp=rospy.Time.now()
        steer_com=NormalizedSteeringCommand(h,1.0)
        speed_pub.publish(speed_com)
        steer_pub.publish(steer_com)
        rate.sleep()

if __name__=='__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass
