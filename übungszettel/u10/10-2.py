import matplotlib.pyplot as plt

#area of calculation
minX = -1
maxX = 5
minY = -1
maxY = 5

#points
startX = 0
startY = 0
obstacleX = 1
obstacleY = 1
goalX = 3
goalY = 4

l = 1

plt.plot(startX, startY, 'kx', obstacleX, obstacleY, 'rs', goalX, goalY, 'g^')
for x in range(minX, maxX):
    for y in range(minY, maxY):
        try:
            plt.text(x, y, ((x - goalX)**2 + (y - goalY)**2) + l * (1 / ((x - obstacleX)**2 + (y - obstacleY)**2)))
        except:
            pass

plt.axis([minX, maxX, minY, maxY])
plt.grid(True)
plt.show()
